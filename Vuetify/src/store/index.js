import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    municipios: [],
    proveedores: [],
    materiales: [],
  },
  mutations: {
    setMunicipios(state, payload){
      state.municipios = payload;
    },
    setProveedores(state, payload) {
      state.proveedores = payload;
    },
    pushProveedores(state, payload){
      state.proveedores.push(payload);
    },
    setMateriales(state, payload) {
      state.materiales = payload;
    },
    pushMateriales(state, payload){
      state.materiales.push(payload);
    },
  },
  actions: {
    //acción para obtener los municipios de colombia
    async getMunicipios({commit}){
      const peticion = await fetch('https://www.datos.gov.co/resource/xdk5-pm3f.json');
      const municipios = await peticion.json();
      commit('setMunicipios', municipios);
    },

    async cargarProveedores({ commit }) {
      const peticion = await fetch('http://localhost:3000/users');
      const data = await peticion.json();
      commit('setProveedores', data);
      //console.table(data);
    },
    async cargarMateriales({ commit }) {
      const peticion = await fetch('http://localhost:3000/material');
      const data = await peticion.json();
      commit('setMateriales', data);
      //console.table(data);
    },
    async crearProveedor({commit}, objProveedor){
      const peticion = await fetch('http://localhost:3000/users', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objProveedor)
      });
      //Capturamos la persona recién insertada
      const data = await peticion.json();
      commit('pushProveedor', data);
    },
    async crearMaterial({commit}, objMaterial){
      const peticion = await fetch('http://localhost:3000/material', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objMaterial)
      });
      //Capturamos la persona recién insertada
      const data = await peticion.json();
      commit('pushMaterial', data);
    },
    async actualizarMaterial({commit}, objMaterial){
      const peticion = await fetch('http://localhost:3000/material', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objMaterial)
      });
      //Capturamos la persona recién insertada
      const data = await peticion.json();
      commit('pushMaterial', data);
    },
    async eliminarProveedor({commit}, obj){
      const peticion = await fetch('http://localhost:3000/users', {
        method: 'DELETE',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      });

    },
    async eliminarMaterial({commit}, obj){
      const peticion = await fetch('http://localhost:3000/material', {
        method: 'DELETE',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      });

    }
  },
  modules: {
  }
})
