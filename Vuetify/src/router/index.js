import Vue from 'vue'
import VueRouter from 'vue-router'
import Proveedores from '../views/Proveedores.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Page',
    component: () => import(/* webpackChunkName: "about" */ '../views/Page.vue') 
  },

  {
    path:'/Proveedores',
    name:'Proveedores',
    component: () => import(/* webpackChunkName: "about" */ '../views/Proveedores.vue')
  }, 
  {
  path:'/Opciones',
  name:'Opciones',
  component: () => import(/* webpackChunkName: "about" */ '../views/Opciones.vue')
  },

  {
    path:'/Consulta',
    name:'Consulta',
    component: () => import(/* webpackChunkName: "about" */ '../views/Consulta.vue')
  },  
  {
    path:'/Login',
    name:'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }, 
  {
    path:'/Galeria',
    name:'Galeria',
    component: () => import(/* webpackChunkName: "about" */ '../views/Galeria.vue')
  },   
  {
    path:'/GestionMat',
    name:'GestionMat',
    component: () => import(/* webpackChunkName: "about" */ '../views/GestionMat.vue'),
    props: true
  }, 
  {
    path:'/GestionProv',
    name:'GestionProv',
    component: () => import(/* webpackChunkName: "about" */ '../views/GestionProv.vue')
  }, 
  

]

const router = new VueRouter({
  routes
})

export default router
